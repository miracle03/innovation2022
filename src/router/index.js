import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/login')
    },
    {
      path: '/user',
      name: 'home',
      component: () => import('@/views/home/index1')
    },
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/source',
      name: 'home',
      component: () => import('@/views/home/index'),
      // children: [
      //   {
      //     path: '/resource',
      //     component: {
      //       resource: () => import('@/views/resource-manage/resource-list')
      //     }
      //   }
      // ]
    },
    {
      path: "/resourcelist",
      component: () => import('@/views/resource-manage/resource-list')
    }
    // {
    //   path: '/404',
    //   component: () => import('@/view/error-page/404'),
    //   hidden: true
    // },
    // { path: '*', redirect: '/404', hidden: true }
  ]
})
