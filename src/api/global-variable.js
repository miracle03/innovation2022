import { getWeb3 } from "C:/Users/81437/Downloads/drst-front-feature-chenjinqian/drst-front-feature-chenjinqian/src/api/web3-api.js";

const ABI_FILE = require("C:/Users/81437/Downloads/drst-front-feature-chenjinqian/drst-front-feature-chenjinqian/UserManage.json")
const CONTRACT_ADDR = "0x30568D37A6473974801Bb9673018107A0922844B"
const web3 = getWeb3("http://10.112.53.99:8545");

const contract = new web3.eth.Contract(
    ABI_FILE,
    CONTRACT_ADDR
);

export default {
    web3,
    contract
}
