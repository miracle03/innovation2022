// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import Web3 from 'C:/Users/81437/Downloads/drst-front-feature-chenjinqian/drst-front-feature-chenjinqian/node_modules/web3/dist/web3.min.js';
import globalVariable from './api/global-variable.js'

Vue.prototype.Web3 = Web3
Vue.prototype.GLOBAL = globalVariable
Vue.config.productionTip = false
Vue.use(ElementUI)
// Vue.prototype.Web3 = Web3
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
